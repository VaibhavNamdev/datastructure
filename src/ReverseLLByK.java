import java.util.ArrayList;


public class ReverseLLByK {
	public static void main(String[] args) {
		InsertDataLL idl = new InsertDataLL();
		LLNode head = idl.setDataLL();
		LLLenght ll = new LLLenght();
		ArrayList al = ll.printList(head);
		System.out.println("Before reversing: "+ al.toString());
		head = reverseLLByKAlernate(head, 4);
		System.out.println("After reversing: "+ ll.printList(head).toString());
	}
	
	public static LLNode reverseLL(LLNode head){
		LLNode head_nxt = null;
		LLNode head_prev = null;
		LLNode h = head;
		while(h!=null){
			head_nxt = h.getNext();
			h.setNext(head_prev);
			head_prev = h;
			h = head_nxt;
		}
		return head_prev;
	}
	
	public static LLNode head = null;
	public static LLNode reverseLLRecursive(LLNode current){
		if(current == null)
			return null;
		if(current.getNext() == null){
			head = current;
			return null;
		}
		reverseLLRecursive(current.getNext());
		current.getNext().setNext(current);
		current.setNext(null);
		return head;
	}
	
	public static LLNode reverseLLByK(LLNode head, int k){
		int x = k;
		LLNode head_nxt = null;
		LLNode head_prev = null;
		LLNode h = head;
		while(h!=null && x > 0){
			head_nxt = h.getNext();
			h.setNext(head_prev);
			head_prev = h;
			h = head_nxt;
			x--;
		}
		if(head_nxt!=null){
			head.setNext(reverseLLByK(head_nxt, k));
		}
		return head_prev;
	}
	
	public static LLNode reverseLLByKAlernate(LLNode head, int k){
		int x = k;
		LLNode head_nxt = null;
		LLNode head_prev = null;
		LLNode h = head;
		while(h!=null && x > 0){
			head_nxt = h.getNext();
			h.setNext(head_prev);
			head_prev = h;
			h = head_nxt;
			x--;
		}
		x = k;
		head.setNext(head_nxt);
//		while(x>0 && head_nxt!=null){
//			head = head_nxt;
//			head_nxt = head_nxt.getNext();
//			x--;
//		}
		if(head_nxt!=null){
			head.setNext(reverseLLByK(head_nxt, k));
		}
		return head_prev;
	}
}
