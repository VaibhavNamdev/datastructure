
public class Twitter {
	public static void main(String[] args) {
		System.out.println(getIntegerComplement(5));
		System.out.println(getIntegerComplement(50));
		int[] xs = {73,48,95,95,33,47,98,91,95,93,70,85,33,47,95,84,43,95,54,72};
		int[] ys = {72,67,92,95,59,58,95,94,84,83,70,79,67,73,87,86,63,92,80,76};
		int[] zs = {76,76,95,96,79,74,97,97,90,90,78,91,76,90,95,95,75,100,87,90};
		System.out.println(corelation(xs, ys));
		System.out.println(corelation(ys, zs));
		System.out.println(corelation(zs, xs));
		
	}
	
	 public static int getIntegerComplement(int n){
		 if (n == 0) {
		        return ~n;
		    }
		    if (n == 1) {
		        return 0;
		   } 
		 int ones = (Integer.highestOneBit(n) << 1) - 1;
		 return n ^ ones;
	 }
	 
	 public static double corelation(int[] xs, int[] ys){
		 	double sx = 0.0;
		    double sy = 0.0;
		    double sxx = 0.0;
		    double syy = 0.0;
		    double sxy = 0.0;

		    int n = xs.length;

		    for(int i = 0; i < n; ++i) {
		      double x = xs[i];
		      double y = ys[i];

		      sx += x;
		      sy += y;
		      sxx += x * x;
		      syy += y * y;
		      sxy += x * y;
		    }

		    // covariation
		    double cov = sxy / n - sx * sy / n / n;
		    // standard error of x
		    double sigmax = Math.sqrt(sxx / n -  sx * sx / n / n);
		    // standard error of y
		    double sigmay = Math.sqrt(syy / n -  sy * sy / n / n);

		    // correlation is just a normalized covariation
		    double roundOff = Math.round((cov / sigmax / sigmay) * 100.0) / 100.0;
		    return roundOff;
	 }
	 
	 static String[] Correlation(String[] scores) {
	        
	        int[] m = new int[scores.length];
	        int[] p = new int[scores.length];
	        int[] c = new int[scores.length];
	        
	        int i = 0;
	        for( String score : scores ){
	            
	            String[] arr = score.split(" ");
	            m[i] = Integer.parseInt(arr[0]);
	            p[i] = Integer.parseInt(arr[1]);
	            c[i] = Integer.parseInt(arr[2]);
	            i++;
	        }
	        
	        double mpRes = corelation(m,p);
	        double pcRes = corelation(p,c);
	        double cmRes = corelation(c,m);
	        
	        String[] res = new String[3];
	        res[0] = String.valueOf(mpRes);
	        res[1] = String.valueOf(pcRes);
	        res[2] = String.valueOf(cmRes);
	        
	        return res;

	    }

}
