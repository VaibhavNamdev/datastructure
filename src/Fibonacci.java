
public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printFibonacci(15);
		System.out.println("\nRecursive --> "+new Fibonacci().printFibonacciRec(15));
		System.out.println("\nDynamic --> "+new Fibonacci().printFibonacciRec(15));
	}
	
	public static void printFibonacci(int range){
		int prev = 0;
		int sum;
		int start = 1;
		if(range == 1){
			System.out.println(prev);
			return;
		}
		System.out.print(prev+" "+start);
		for (int i = 2; i <= range ; i++){
			sum = start+prev;
			prev = start;
			start = sum;
			System.out.print(" "+sum);
		} 
	}
	
	public int printFibonacciRec(int n){
		if(n <= 1){
			return n;
		}
		
			return printFibonacciRec(n - 1) + printFibonacciRec(n -2);
	}
	
	public int dynamicFibo(int n){
		int[] fib = new int[n+1];
		int i;
		fib[0] = 0;
		fib[1] = 1;
		for(i=2; i<=n; i++){
			fib[i] = fib[i-1]+fib[i-2];
		}
		return fib[n];
	}

}
