package Utility;

public class Power {
	public static void main(String[] args) {
		System.out.println(pow2(3, 3));
	}
	
	public static double pow(int x, int n){
		double result = 1;
		for (int i = 0; i < n; i++) {
			result = result * x;
		}
		
		return result;
	}
	
	public static double pow2(int x, int n){
		if(n<0)
			return power(x, -n);
		else
			return power(x, n);
	}
	
	private static double power(int x, int n){
		if(n == 0)
			return 1;
		
		double v = power(x, n/2);
		
		if(n%2 == 0)
			return v*v;
		else
			return v*v*x;
	}
	
	
	
}
