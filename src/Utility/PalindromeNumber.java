package Utility;

public class PalindromeNumber {
	public static void main(String[] args) {
		int num = -123456;
		System.out.println("Number "+num+" is Palindore ? " + isPalindrome(num));
	}
	
	public static boolean isPalindrome(int num){
		boolean abs = false;
		int i = Math.abs(num), j=0;
		
		if(num<0)
			abs = true;

		while(i!=0){
			int temp = i%10;
			i = i/10;
			j = j*10+temp;
		}
		System.out.println(abs ?  j*-1 : j);
		return Math.abs(num) == j ? true:false;
	}
}
