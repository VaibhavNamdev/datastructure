
public class LLNode {
	private LLNode next;
	private int data;
	
	public void setData(int data){
		this.data = data;
	}
	public int getDataI(){
		return this.data;
	}
	public void setNext(LLNode nxt){
		this.next = nxt;
	}
	public LLNode getNext(){
		return this.next;
	}
	
}
