package Bitwise;

public class MaxBinaryGap {
	public static void main(String[] args) {
		int n = 9;  // check with 523 will give 5 gap as 001000001011 is binary of 523
		System.out.println("Maximum Binary gap in "+n+" is ==> "+maxBinaryGap(n));
	}
	
	public static int maxBinaryGap(int n){
		int count = -1;
		int max = 0;
		int r;
		while(n>0){
			r = n & 1;
			n = n >> 1;
			
			if(r==0 && count >=0)
				count++;
			else{
				max = count > max ? count : max;
				count = 0;
			}
		}
		return max;
	}
}
