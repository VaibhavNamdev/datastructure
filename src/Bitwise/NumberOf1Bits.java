package Bitwise;

public class NumberOf1Bits {
	public static void main(String[] args) {
		int n = 5;
		System.out.println("Total number of one bits in "+n+" is ==> "+countOneBits(n));
	}
	
	public static int countOneBits(int n){
		int count = 0;
		while(n>0){
			int r = n&1;
			n = n>>1;
			if(r == 1 )
				count++;
		}
		return count;
	}
}
