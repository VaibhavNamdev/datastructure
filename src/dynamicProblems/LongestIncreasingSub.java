package dynamicProblems;

import java.util.*;

public class LongestIncreasingSub {
	public static void main(String[] args) {
        int A[] = { 2, 5, 3, 7, 11, 8, 10, 13, 6 };
        int n = A.length;
        System.out.println("Length of Longest Increasing Subsequence is "+
                            LIS(A, n));
	}
	
	public static int LIS(int [] A, int n){
		int [] temp = new int[n];
		int len = 1;
		temp[0] = A[0];
		for(int i=1;i<n;i++){
			if(A[i] < temp[0])
				temp[0] = A[i];
			else if(A[i] > temp[len-1])
				temp[len++] = A[i];
			else{
				int ceil = ceilArray(temp, -1, len-1, A[i]);
				temp[ceil] = A[i];
			}		
		}
		return len;
	}
	
	private static int ceilArray(int[] A, int l, int r, int key){
		while(r-l>1){
			int m = l + (r - l)/2;
            if (A[m]>=key)
                r = m;
            else
                l = m;
		}
		return r;
		
	}
}
