package dynamicProblems;

import java.util.Arrays;

public class MinimumCoinChanging {
	public static void main(String[] args) {
		int [] coins = {7,2,3,6};
		int total = 13;
		System.out.println("Minimum Required Coins : ==> "+minCoinChange(coins, total));
	}
	
	public static int minCoinChange(int [] coins, int total){
		int [] table = new int[total+1];
		int [] index = new int[total+1];
		table[0] = 0;
		for(int i= 1; i<table.length; i++){
			table[i] = Integer.MAX_VALUE-1;
			index[i] = -1;
		}	
		
		for(int i = 0; i<coins.length; i++){
			for(int j = 1; j<table.length; j++){
				if(j>=coins[i]){
					if(table[j-coins[i]]<table[j]){
						table[j] = Math.min(table[j], 1 + table[j-coins[i]]);
						index[j] = i;
					}
				}
			}
		}
		System.out.print("Coins Used are : ==> ");
		printCoins(index, coins);
		return table[total];
	}
	
	private static void printCoins(int [] index, int [] coins){
		int i = index.length-1;
		
		while(i>0){
			System.out.print(coins[index[i]]+" ");
			i = i-coins[index[i]];
		}
		System.out.println("");
	}
}
