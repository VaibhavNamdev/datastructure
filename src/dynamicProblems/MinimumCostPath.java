package dynamicProblems;

import java.util.Arrays;

public class MinimumCostPath {
	public static void main(String[] args) {
		int [][] A = new int[][] { { 1, 7, 9, 2 }, { 8, 6, 3, 2 }, { 1, 6, 7, 8 },
				{ 2, 9, 8, 2 } };
		
		System.out.println(minimumCostRecursive(A, A.length-1, A[0].length-1));
		System.out.println(minimumCostDynamic(A, A.length, A[0].length));
	}
	
	public static int minimumCostRecursive(int [][] A, int rows, int columns){
		if(rows<0 || columns<0)
			return Integer.MAX_VALUE;
		if(rows == 0 && columns == 0)
			return A[rows][columns];
		else
			return A[rows][columns] + Math.min(Math.min(minimumCostRecursive(A, rows-1, columns-1),
					minimumCostRecursive(A, rows-1, columns)), minimumCostRecursive(A, rows, columns-1));
	}
	
	
	public static int minimumCostDynamic(int [][] A, int rows, int columns){
		int [][]sumMatrix = new int[rows][columns];
		
		sumMatrix[0][0] = A[0][0];
		
		//sum and save row 0
		for(int i=1; i<columns; i++)
			sumMatrix[0][i] = A[0][i] + sumMatrix[0][i-1];
		
		//sum and save column 0
		for(int i=1; i<rows; i++)
			sumMatrix[i][0] = A[i][0] + sumMatrix[i-1][0];
		
		for(int i=1; i<rows; i++){
			for(int j=1; j<columns; j++){
				sumMatrix[i][j] = A[i][j] + Math.min(Math.min(sumMatrix[i-1][j-1],sumMatrix[i-1][j]), sumMatrix[i][j-1]);
			}
		}

		return sumMatrix[rows-1][columns-1];
		
	}
}