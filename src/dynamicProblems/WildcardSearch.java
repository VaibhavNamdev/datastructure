package dynamicProblems;

public class WildcardSearch {
	public static void main(String[] args) {
		String s = "xaylmz";
		String p = "x?y*z";
		char [] str = s.toCharArray();
		char [] pattern = p.toCharArray();
		
		System.out.println("Is String matched to Wildcard ==> "+ isStringMatchWildcard(str, pattern));
	}
	
	// * --> 0 or more character
	// ? --> exactly one character
	public static boolean isStringMatchWildcard(char [] str, char [] pattern){
		boolean [][] table = new boolean[str.length+1][pattern.length+1];
		table[0][0] = true;
		
		for(int i=1; i<pattern.length+1; i++)
			table[0][i] = false;
		
		for(int i=1; i<str.length+1; i++)
			table[i][0] = false;
		
		for(int i=1; i<str.length+1; i++){
			for(int j=1; j<pattern.length+1; j++){
				if(pattern[j-1] == '?' || str[i-1] == pattern[j-1]){
					table[i][j] = table[i-1][j-1];
				}else if(pattern[j-1] == '*'){
					table[i][j] = table[i][j-1] || table[i-1][j];
				}
			}
		}
		
		return table[str.length][pattern.length];
	}
}
