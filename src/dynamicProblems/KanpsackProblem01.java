package dynamicProblems;

public class KanpsackProblem01 {
	public static void main(String[] args) {
		int []val = {60,100,120};
		int []wt = {10,20,30};
		int W = 50;
		System.out.println("Recursive -->"+kanpsackDynamic(val, wt, W));
		System.out.println("Dyanmic -->"+kanpsackDynamic(val, wt, W));
	}
	
	public static int kanpsackRecursive(int []val, int[] wt, int l,int W){
		if(W==0 && l==0)
			return 0;
		if(W<wt[l-1])
			return kanpsackRecursive(val, wt, l-1, W);
		else
			return Math.max(val[l-1]+kanpsackRecursive(val, wt, l-1, W-wt[l-1]), kanpsackRecursive(val, wt, l-1, W));
	}
	
	public static int kanpsackDynamic(int []val, int[] wt, int W){
		int [][] table = new int[val.length+1][W+1];
		
		for(int i=0; i<val.length+1; i++)
			table[i][0] = 0;
		
		for(int i=1; i<W+1; i++)
			table[0][i] = 0;
		
		
		for(int i=1; i<=val.length; i++){
			for(int j=1; j<=W; j++){
				if(j<wt[i-1])
					table[i][j] = table[i-1][j];
				else{
					table[i][j] = Math.max((val[i-1]+table[i-1][j-wt[i-1]]), table[i-1][j]);
				}
			}
		}
		return table[val.length][W];
	}
}
