package dynamicProblems;

public class CoinProblem {
	public static void main(String[] args) {
		int A[] = {1,2,3};
		System.out.println(countCoinChange(A, A.length, 20));
		System.out.println(countCoinDynamic(A, A.length, 20));
		System.out.println(countCoinDynamic2(A, A.length, 20));
		System.out.println(minCoinProblem(A, A.length, 20));
		System.out.println(minCoinDynamic(A, A.length, 20));
	}
	
	public static int countCoinChange(int [] A, int n, int sum){
		if(sum == 0)
			return 1;
		if(sum<0)
			return 0;
		if(n<=0 && sum>=1)
			return 0;
		return countCoinChange(A, n-1, sum) + countCoinChange(A, n, sum - A[n-1]);
	}
	
	public static int countCoinDynamic(int []A, int n, int sum){
		int [] table = new int[sum+1];
		table[0] = 1;
		for(int i=1;i<sum+1;i++)
			table[i] = 0;
		for(int i=0; i<n; i++){
			for(int j=A[i];j<sum+1;j++){
				table[j] += table[j-A[i]];
			}
		}
		return table[sum];
	}
	
	public static int countCoinDynamic2(int [] coins, int length, int amount){
		int solution[][] = new int[length+1][amount+1];
		for(int i=0; i<length+1;i++)
			solution[i][0] = 1;
		for(int i=1; i<amount+1;i++)
			solution[0][i] = 0;
		for(int i=1; i<=length;i++){
			for(int j=1; j<=amount; j++ ){
				if(coins[i-1]<=j){
					solution[i][j] = solution[i-1][j] + solution[i][j-coins[i-1]];
				}
				else{
					solution[i][j] = solution[i-1][j];
				}
			}
		}
		return solution[length][amount];
	}
	
	public static int minCoinProblem(int [] coin, int m, int V){
		if(V == 0)
			return 0;
		
		int res = Integer.MAX_VALUE;
		
		for(int i=0; i<m ; i++){
			if(coin[i] <= V){
				int sub_res = minCoinProblem(coin, m, V-coin[i]);
				
				if(sub_res != Integer.MAX_VALUE && (sub_res+1)<res)
					res = sub_res + 1;
			}
		}
		return res;
	}
	
	public static int minCoinDynamic(int [] coin, int m, int V){
		int [] table = new int[V+1];
		table[0] = 0;
		for(int i=1; i<=V; i++)
			table[i] = Integer.MAX_VALUE;
		for(int i=1;i<=V;i++){
			for(int j=0;j<m;j++){
				if(coin[j]<=i){
					int sub_res = table[i-coin[j]];
					if(sub_res!=Integer.MAX_VALUE && sub_res+1 < table[i])
						table[i] = sub_res + 1;
				}
			}
		}
		return table[V];
	}
}
