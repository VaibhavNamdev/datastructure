package dynamicProblems;

public class LongestComSub {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String A = "AGGTAB";
		String B =  "GXTXAYB";
		char [] X = A.toCharArray();
		char [] Y = B.toCharArray();
		
		System.out.println(lcs(X, Y, X.length, Y.length));
		System.out.println(lcsDynami(X, Y, X.length, Y.length));
		System.out.println(lcsDynamicSelf(X, Y, X.length, Y.length));

	}
	
	public static int lcs(char [] A, char [] B, int m, int n){
		if(m==0 || n==0)
			return 0;
		if(A[m-1] == B[n-1])
			return 1 + lcs(A, B, m-1, n-1);
		return Math.max(lcs(A, B, m, n-1), lcs(A, B, m-1, n));
	}
	
	public static int lcsDynami(char [] A, char [] B, int m, int n){
		int L[][] = new int[m+1][n+1];
		int i,j;
		for(i=0;i<=m;i++){
			for(j=0;j<=n;j++){
				if(i==0 || j==0)
					L[i][j] = 0;
				else if(A[i-1] == B[j-1])
					L[i][j] = L[i-1][j-1]+1;
				else
					L[i][j] = Math.max(L[i-1][j], L[i][j-1]);
			}
		}
		return L[m][n];
	}
	
	
	public static int lcsDynamicSelf(char[] A, char [] B, int lenA, int lenB){
		int table[][] = new int[lenA+1][lenB+1];
		for(int i=0;i<=lenA;i++)
			table[i][0]=0;
		for(int i=1;i<=lenB;i++)
			table[0][i]=0;
		
		for(int i=1;i<=lenA;i++){
			for(int j=1;j<=lenB;j++){
				if(A[i-1] == B[j-1])
					table[i][j] = table[i-1][j-1] + 1;
				else
					table[i][j] = Math.max(table[i-1][j], table[i][j-1]);
			}
		}
		
		return table[lenA][lenB];
	}
	

}
