package dynamicProblems;

public class EditDistance {
	public static void main(String[] args) {
		String s1= "SUNDAY";
		String s2 = "SATURDAY";
		char [] A = s1.toCharArray();
		char [] B = s2.toCharArray();
		System.out.println(editDistance(A, B, A.length, B.length));
		System.out.println(editDistanceDynamic(A, B, A.length, B.length));
		
	}
	
	public static int editDistance(char[] A, char[] B, int m, int n){
		if(m==0)
			return n;
		if(n==0)
			return m;
		if(A[m-1] == B[n-1])
			return editDistance(A, B, m-1, n-1);
		else{
			return 1 + Math.min(Math.min(editDistance(A, B, m, n-1), 
					editDistance(A, B, m-1, n)), 
					editDistance(A, B, m-1, n-1));
		}
	}
	
	public static int editDistanceDynamic(char [] A, char[] B, int m, int n){
		int L[][] = new int[m+1][n+1];
		
		for(int i=0; i<=m; i++){
			for(int j=0; j<=n; j++){
				if(i==0)
					L[i][j] = j;
				else if(j==0)
					L[i][j] = i;
				else if(A[i-1] == B[j-1])
					L[i][j] = L[i-1][j-1];
				else
					L[i][j] = 1 + Math.min(Math.min(L[i][j-1], L[i-1][j]), L[i-1][j-1]);
			}
		}
		
		return L[m][n];
	}
}
