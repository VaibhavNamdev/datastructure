package dynamicProblems;

public class SubsetSum {
	public static void main(String[] args) {
		int [] A = {1,6,7,9};
		int sum = 8;
		System.out.println(subsetSum(A,A.length,sum));
		System.out.println(subsetSumDynamic(A, A.length, sum));
	}
	
	public static boolean subsetSum(int [] A, int n, int sum){
		if(sum == 0)
			return true;
		if(n==0 && sum!=0)
			return false;
		if(A[n-1]>sum)
			return subsetSum(A,n-1,sum);
		
		return subsetSum(A, n-1, sum) || subsetSum(A,n-1,sum-A[n-1]);	
	}
	
	public static boolean subsetSumDynamic(int [] A, int n, int sum){
		boolean solution[][] = new boolean[n+1][sum+1];
		for(int i=0; i<n+1; i++)
			solution[i][0] = true;
		for(int i=1; i<sum+1;i++)
			solution[0][i] = false;
		
		for(int i=1; i<=n; i++){
			for(int j=1; j<=sum; j++){
				solution[i][j] = solution[i-1][j];
				if(A[i-1]<=j){
					solution[i][j] = solution[i][j] || solution[i-1][j-A[i-1]];
				}
			}
		}
		return solution[n][sum];
	}
	
}
