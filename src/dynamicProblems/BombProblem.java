package dynamicProblems;

import java.util.*;

public class BombProblem {
	public static void main(String[] args) {
		int[] A= {1,2,2,3,3,4,5,5,6,6,6,7,7};
		System.out.println(conatinsBomb(A));
	}
	
	public static boolean conatinsBomb(int A[]){
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		boolean result = false;
		for(int i=0;i<A.length;i++){
			if(map.containsKey(A[i])){
				map.put(A[i], map.get(A[i])+1);
			}
			else
				map.put(A[i], 1);
		}
		
		int count=0, previous=-1, current;
		boolean processed=false;
		
		for(int i=0; i<A.length;i++){
			int value = map.get(A[i]);
			
			if(previous!=A[i]){
				processed =  false;
			}
			
			if( (previous!= A[i]) && (value>1 && !processed)){
				processed = true;
				if(previous != (A[i]-1))
					count=0;
				count++;
			}
			
			if(count>=3){
				result = true;
				break;
			}
			
			previous = A[i];
		}
		
		return result;
	}
}
