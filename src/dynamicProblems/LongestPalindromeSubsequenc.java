package dynamicProblems;

public class LongestPalindromeSubsequenc {
	public static void main(String[] args) {
		String s = "helloello";
		char [] word = s.toCharArray();
		int len = word.length-1;
		System.out.println(lpsRecursive(word, 0, len));
		System.out.println(lpsDynamic(word, word.length));
		System.out.println("\n\nLongest Palindrome Substring\n"+lpSubStringDynamic(word, word.length));
	}

	public static int lpsRecursive(char [] word, int i, int j){
		if(i==j){
			return 1;
		}
		if(word[i] == word[j] && i+1 == j){
			return 2;
		}
		if(word[i] == word[j]){
			return lpsRecursive(word, i+1, j-1)+2;
		}
		else
			return Math.max(lpsRecursive(word, i+1, j), lpsRecursive(word, i, j-1));
	}
	
	public static int lpsDynamic(char [] word, int len){
		int [][] result = new int[len][len];
		
		for(int i=0; i<len; i++)
			result[i][i] = 1;
		
		for(int sublen=1; sublen<len; sublen++){
			for(int i=0; i<(len-sublen); i++){
				int j = i+sublen;
				if(word[i]==word[j] && sublen == 1)
					result[i][j] = 2;
				else if(word[i] == word[j])
					result[i][j] = 2 + result[i+1][j-1];
				else
					result[i][j] = Math.max(result[i][j-1], result[i+1][j]);
			}
			
		}
		return result[0][len-1];		
	}
	
	public static int lpSubStringDynamic(char[] word, int len){
		boolean [][] result = new boolean[len][len];
		int start = 0, length = 1;
		
//		for(int i=0;i<len;i++){
//			for(int j=0;j<len;j++){
//				result[i][j] = false;
//			}
//		}
		
		for(int i=0; i<len; i++)
			result[i][i] = true;
		
		for(int i=0; i<len-1;i++){
			if(word[i]==word[i+1]){
				result[i][i+1] = true;
				start = i;
				length = 2;
			}
		}
		
		for(int sublen=2; sublen<len; sublen++){
			for(int i=0; i<(len-sublen); i++){
				int j = i+sublen;
				if(word[i]==word[j] && result[i+1][j-1]){
					result[i][j] = true;
					if(sublen>length){
						start = i;
						length = sublen;
					}
				}
					
			}
		}
		
		for(int i=0; i<=length; i++){
			System.out.print(word[start++]);
		}
		return length;
	}
}
