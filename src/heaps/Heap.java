package heaps;

public class Heap {
	public int [] array;
	public int capacity;
	public int count;
	public int heap_type;
	
	public Heap(int heap_type){
		this.count=0;
		this.heap_type = heap_type;
	}
	
	public int parent(int i){
		if(i<0 || i>this.count)
			return -1;
		if(i!=0){
			return ((i-1)/2);
		}
		return i;
	}
	
	public int lChild(int i){
		int left = 2*i+1;
		if(left>=this.count)
			return -1;
		return left;
	}
	
	public int rChild(int i){
		int right = 2*i+2;
		if(right>=this.count)
			return -1;
		return right;
	}
	
	public int GetMaximum(){
		if(this.heap_type == 1){
			if(this.count != 0)
				return this.array[0];
		}
		return -1;
	}
	
	public int GetMinimum(){
		if(this.heap_type == 0){
			if(this.count != 0)
				return this.array[0];
		}
		return -1;
	}
	
	public void perlocateDown(int i){
		int l, r, max, temp;
		l = lChild(i);
		r = rChild(i);
		if(l!=-1 && array[l]>array[i])
			max = l;
		else
			max= i;
		if(r!=-1 && array[r]>array[max])
			max = r;
		if(max!=i){
			temp = array[i];
			array[i] = array[max];
			array[max] = temp;
			perlocateDown(max);
		}
		
		
	}
	
	public void perlocateDownMin(int i){
		int l, r, min, temp;
		l = lChild(i);
		r = rChild(i);
		if(l!=-1 && array[l]<array[i])
			min = l;
		else
			min= i;
		if(r!=-1 && array[r]<array[min])
			min = r;
		if(min!=i){
			temp = array[i];
			array[i] = array[min];
			array[min] = temp;
			perlocateDownMin(min);
		}
		
		
	}
}
