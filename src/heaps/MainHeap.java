package heaps;

import java.util.Arrays;

public class MainHeap {
	public static void main(String[] args) {
		Heap heap = setHeap();
		
		heapifyMin(heap);
		System.out.println("Heapify Min heap--> "+ Arrays.toString(heap.array));
		heapSort(heap,1);		
		System.out.println("Sorting Max->Min --> "+Arrays.toString(heap.array));
		
		heapify(heap);
		System.out.println("\nHeapify Max heap--> "+ Arrays.toString(heap.array));
		heapSort(heap,0);	
		System.out.println("Sorting Min->Max --> "+Arrays.toString(heap.array));
		
	}
	
	public static Heap setHeap(){
		Heap heap = new Heap(1);
		int [] a= {31,1,21,5,8,12,18,3,2,10,7};
		
		heap.count = a.length;
		heap.array = a;
		heap.count = a.length;
		return heap;
	}
	
	public static void heapify(Heap h){
		if(h==null)
			return;
		for(int i=(h.array.length/2)-1;i>=0;i--)
			h.perlocateDown(i);
	}
	
	public static void heapifyMin(Heap h){
		if(h==null)
			return;
		for(int i=(h.array.length/2)-1;i>=0;i--)
			h.perlocateDownMin(i);
	}
	
	public static void heapSort(Heap h, int type){
		int temp;
		int oldCount = h.count;
		for(int i=h.count-1; i>=0; i-- ){
			temp = h.array[0];  
			h.array[0] = h.array[h.count-1];
			h.array[h.count-1] = temp;
			h.count--;
			if(type==1){
				h.perlocateDownMin(0);
			}else
				h.perlocateDown(0);
		}
		
		h.count = oldCount;
	}
}
