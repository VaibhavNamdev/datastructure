import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;


public class Brackets {
	public static int internal = 0;
	public static void main(String[] args) {
		int size = 6;
	    int numRows = (int)Math.pow(2, size);
	    int[][] bools = new int[numRows][size];
	    if(size%2!=0){
	    	System.out.println("Not Possible");
	    	System.exit(0);
	    }
	    for(int i = 0;i<bools.length;i++)
	    {
	        for(int j = 0; j < bools[i].length; j++)
	        {
	            int val = bools.length * j + i;
	            int ret = (1 & (val >>> j));
	            bools[i][j] = ret != 0 ? 1 : 0;
//	            System.out.print(bools[i][j] + "\t");
	        }
//	        System.out.println();
	    }
	    System.out.println(numRows);
	    System.out.println("\n");
	    int [] temp = new int[size];
	    int count = 0;
	    for(int i =0 ; i<numRows; i++){
    		boolean check = true;
	    	for(int j=0; j<size ; j++){
	    		if(bools[i][0] > 0 || bools[i][size-1] < 1){
	    			check = false;
	    			break;
	    		}else{
	    			temp[j] = bools[i][j]; 
	    		}
	    	}
	    	if(check && isMatched(temp, size)){
	    		System.out.println("Found : ==> "+Arrays.toString(temp));
	    		count++;
	    	}
	    }
	    System.out.println("Total possible : ==> "+count);
	    System.out.println("Internal ==> "+internal + " Total ==> "+numRows);
	}
	
	public static boolean isMatched(int [] A, int len){
		internal ++;
		Stack<Integer> s = new Stack<Integer>();
		boolean flag = false;
		s.push(A[0]);
		for(int i=1; i<len; i++){
			if(s.isEmpty() && A[i] == 1){
				flag = false;
				return flag;
			}
			else if(!s.isEmpty() && s.peek() < A[i]){
				s.pop();
			}else{
				s.push(A[i]);
			}
		}
		
		if(s.isEmpty()){
			flag = true;
		}
		return flag;
	}
	
}
