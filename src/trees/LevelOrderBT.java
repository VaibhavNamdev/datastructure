package trees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class LevelOrderBT {
	public ArrayList<Integer> LevelOrderTraversal(BinaryTreeNode root){
		ArrayList<Integer> result = new ArrayList<Integer>();
		Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
		if(root==null)
			return result;
		int MaxValue = Integer.MIN_VALUE;
		int Depth = 0;
		queue.add(root);
		queue.add(null);
		while(!queue.isEmpty()){
			BinaryTreeNode temp = queue.poll();
			if(temp!=null){
				result.add(temp.getData());
				if(temp.getData()>MaxValue){
					MaxValue = temp.getData();
				}
				if(temp.getLeft()!=null){
					queue.add(temp.getLeft());
				}
				if(temp.getRight()!=null){
					queue.add(temp.getRight());
				}
			}
			else{
				Depth++;
				if(!queue.isEmpty()){
					queue.add(null);
				}
			}
		}
		System.out.println("Max Value --> "+MaxValue);
		System.out.println("Depth Of Tree --> "+Depth);
		return result;
	}
	
	public int FindMaxRecursive(BinaryTreeNode root){
		int MaxValue = Integer.MIN_VALUE;
		if(root!=null){
			int leftMax = FindMaxRecursive(root.getLeft());
			int rightMax = FindMaxRecursive(root.getRight());
			
			if(leftMax > rightMax)
				MaxValue = leftMax;
			else
				MaxValue = rightMax;
			if(root.getData() > MaxValue)
				MaxValue = root.getData();
		}
		return MaxValue;
	}
	
	public boolean SearchBT(BinaryTreeNode root, int data){
		Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
		boolean result = false;
		if(root==null)
			return result;
		int MaxValue = Integer.MIN_VALUE;
		queue.add(root);
		while(!queue.isEmpty()){
			BinaryTreeNode temp = queue.poll();
			if(temp.getData() == data){
				result = true;
				break;
			}
			if(temp.getLeft()!=null){
				queue.add(temp.getLeft());
			}
			if(temp.getRight()!=null){
				queue.add(temp.getRight());
			}
		}
		return result;
	}
	
	public int SizeofBTRecursive(BinaryTreeNode root){
		int leftCount = root.getLeft()==null ? 0 : SizeofBTRecursive(root.getLeft());
		int rightCount = root.getRight()==null ? 0 : SizeofBTRecursive(root.getRight());
		return 1 + leftCount + rightCount;
	}
	
	public int DepthofBTRecursive(BinaryTreeNode root){
		if(root == null)
			return 0;
		
		int leftHeight = root.getLeft() == null ? 0 : DepthofBTRecursive(root.getLeft());
		int rightHeight = root.getRight() == null ? 0 : DepthofBTRecursive(root.getRight());
		
		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	}
	
	public static int heightOfTree(BinaryTreeNode root){
		if(root == null)
			return 0;
		
		return (Math.max(heightOfTree(root.getLeft()), heightOfTree(root.getRight()))+1);
	}
	
//	private int diameter = 0;
//	public int diameterOfTree(BinaryTreeNode root){
//		int left, right;
//		if(root == null)
//			return 0;
//		
//		left = diameterOfTree(root.getLeft());
//		right = diameterOfTree(root.getRight());
//		if(left+right > diameter)
//			diameter = left + right;
//		return (Math.max(left, right)+1);
//		
//	}
	
	public int diameterAlternate(BinaryTreeNode root){
		if(root == null){
			return 0;
		}
		
		int lengthLeft = heightOfTree(root.getLeft());
		int lengthRight = heightOfTree(root.getRight());
		
		int diameterLeft = diameterAlternate(root.getLeft());
		int diameterRight = diameterAlternate(root.getRight());
		
		return (Math.max(lengthLeft+lengthRight+1, Math.max(diameterLeft, diameterRight)));
	}
	
	public void printTreePaths(BinaryTreeNode root){
		int [] paths = new int[256];
		int pathLength = 0;
		printTreePaths(root, paths, pathLength);
	}
	
	private void printTreePaths(BinaryTreeNode root, int [] paths, int pathLength){
		if(root == null )
			return;
		
		paths[pathLength] = root.getData();
		pathLength++;
		if(root.getLeft()==null && root.getRight() == null){
			printArray(paths, pathLength);
			pathLength++;
		}
		else{
			printTreePaths(root.getLeft(), paths, pathLength);
			printTreePaths(root.getRight(), paths, pathLength);
		}
		
	}
	private void printArray(int [] print, int pathLength){
		for (int i = 0; i < pathLength; i++) {
			System.out.print(print[i]+"  ");
		}
		System.out.println("");
	}
	
	public boolean hasPathSum(BinaryTreeNode root, int sum){
		if(root==null)
			return false;
		if(root.getLeft()==null && root.getRight()==null && root.getData() == sum)
			return true;
		else
			return hasPathSum(root.getLeft(), sum-root.getData())||
						hasPathSum(root.getRight(), sum-root.getData());
	}
	
	public boolean printAllAnsestors(BinaryTreeNode root, BinaryTreeNode node){
		if(root == null){
			return false;
		}
		if(root.getLeft()==node || root.getRight() == node ||
				printAllAnsestors(root.getLeft(), node) || printAllAnsestors(root.getRight(), node)){
			System.out.print(root  .getData()+"  ");
			return true;
		}
		return false;
	}
	
	public BinaryTreeNode LeastCommonAnsistor(BinaryTreeNode root, BinaryTreeNode a, BinaryTreeNode b){
		BinaryTreeNode left, right;
		if(root == null)
			return root;
		if(root == a || root == b){
			return root;
		}
		left = LeastCommonAnsistor(root.getLeft(), a, b);
		right = LeastCommonAnsistor(root.getRight(), a, b);
		if(left!= null && right!=null){
			return root;
		}
		else{
			return (left!=null?left:right);
		}
	}
	
}
