package trees;

import java.util.ArrayList;
import java.util.Stack;

public class PreorderBT {
	public void PreorderRecursive(BinaryTreeNode root){
		if(root!=null){
			System.out.print(root.getData()+"  ");
			PreorderRecursive(root.getLeft());
			PreorderRecursive(root.getRight());
		}
	}
	
	public ArrayList<Integer> PreorderIterative(BinaryTreeNode root){
		ArrayList<Integer> result = new ArrayList<Integer>();
		Stack<BinaryTreeNode> temp = new Stack<BinaryTreeNode>();
		temp.push(root);
		while(!temp.isEmpty()){
			BinaryTreeNode top = temp.pop();
			result.add(top.getData());
			if(top.getRight()!=null){
				temp.push(top.getRight());
			}
			if(top.getLeft()!=null){
				temp.push(top.getLeft());
			}
		}
		return result;
			
	}
}
