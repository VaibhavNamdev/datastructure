package trees;

import java.util.ArrayList;
import java.util.List;

public class RootToLeafSum {
	public static List<Integer> result;
	public static void main(String[] args) {
		GenerateBT bt = new GenerateBT();
		result = new ArrayList<Integer>();
		System.out.println(isSumExists(bt.getRoot(), 46, result));
	}
	
	public static boolean isSumExists(BinaryTreeNode root, int sum, List<Integer> result){
		if(root == null)
			return false;
		if(root.getLeft() ==null && root.getRight() == null){
			if(root.getData() == sum){
				result.add(root.getData());
				return true;
			}
			else{
				return false;
			}
		}
		if(isSumExists(root.getLeft(), sum-root.getData(), result)){
			result.add(root.getData());
			return true;
		}
		if(isSumExists(root.getRight(), sum-root.getData(), result)){
			result.add(root.getData());
			return true;
		}
		return false;
	}
}
