package trees;

public class GreaterTree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GenerateBT bt = new GenerateBT();
		greaterTree(bt.getRoot());
		InorderBT in = new InorderBT();
		in.InorderRecursive(bt.getRoot());
	}
	public static int sum = 0;
	public static void greaterTree(BinaryTreeNode root){
		if(root!=null){
			greaterTree(root.getRight());
			int temp = root.getData();
			root.setData(sum);
			sum = sum + temp;
			greaterTree(root.getLeft());
		}
	}
}
