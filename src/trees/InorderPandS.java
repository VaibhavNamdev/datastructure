package trees;

public class InorderPandS {
	public static void main(String[] args) {
		GenerateBT bt = new GenerateBT();
		inorderPreSucc(bt.getRoot(), 50);
	}
	
	public static void inorderPreSucc(BinaryTreeNode root, int data){
		BinaryTreeNode node = searchNode(root, data);
		if(node!=null){
			if(node.getLeft()!=null)
				System.out.println("Inorder Predecessor => "+findMax(node.getLeft()).getData());
			if(node.getRight()!=null){
				System.out.println("Inorder Successor  => "+findMin(node.getRight()).getData());
			}
		}
	}
	
	public static BinaryTreeNode searchNode(BinaryTreeNode root, int data){
		while(root!=null){
			if(root.getData() == data)
				return root;
			else if(root.getData() < data)
				root = root.getRight();
			else
				root = root.getLeft();
		}
		
		return null;
	}
	
	public static BinaryTreeNode findMin(BinaryTreeNode root){
		while(root.getLeft()!=null){
			root = root.getLeft();
		}
		return root;
	}
	
	public static BinaryTreeNode findMax(BinaryTreeNode root){
		while(root.getRight()!=null){
			root = root.getRight();
		}
		return root;
	}
}
