package trees;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

public class LargestBST {
	public static void main(String[] args) {
		GenerateBT bt = new GenerateBT();
		System.out.println(largestBinaryTree(bt.getRoot()));
	}
	
	private static class MinMax{
		public boolean isBst;
		public int minValue;
		public int maxValue;
		public int size;
		public MinMax() {
			// TODO Auto-generated constructor stub
			isBst = true;
			minValue = Integer.MAX_VALUE;
			maxValue = Integer.MIN_VALUE;
			size = 0;
		}
	}
	
	public static int largestBinaryTree(BinaryTreeNode root){
		MinMax m = largest(root);
		return m.size;
	}
	
	private static MinMax largest(BinaryTreeNode root){
		if(root==null){
			return new MinMax();
		}
		
		MinMax ml = largest(root.getLeft());
		MinMax mr = largest(root.getRight());
		MinMax m = new MinMax();
		if(ml.isBst == false || mr.isBst == false || ml.maxValue > root.getData() || mr.minValue < root.getData()){
			m.isBst = false;
			m.size = Math.max(ml.size, mr.size);
			return m;
		}
		
		m.isBst = true;
		m.size = ml.size + mr.size + 1;
		
		m.minValue = root.getLeft()!=null?ml.minValue:root.getData();
		m.maxValue = root.getRight()!=null?mr.maxValue:root.getData();
		
		return m;
		
	}
}
