package trees;

import java.util.ArrayList;
import java.util.Stack;

public class PostOrderBT {
	public void PostOrderRecursive(BinaryTreeNode root){
		if(root != null){
			PostOrderRecursive(root.getLeft());
			PostOrderRecursive(root.getRight());
			System.out.print(root.getData()+"  ");
		}
	}
	public ArrayList<Integer> PostOrderIterative(BinaryTreeNode root){
		if(root == null){
			return new ArrayList<Integer>();
		}
		int maxDepth = Integer.MIN_VALUE;
		ArrayList<Integer> result = new ArrayList<Integer>();
		Stack<BinaryTreeNode> s = new Stack<BinaryTreeNode>();
		s.push(root);
		BinaryTreeNode prev = null;
		while(!s.isEmpty()){
			BinaryTreeNode curr = s.peek();
			if(prev == null || prev.getLeft() == curr || prev.getRight()==curr){
				if(curr.getLeft()!=null){
					s.push(curr.getLeft());
				}
				else if(curr.getRight()!=null){
					s.push(curr.getRight());
				}
			}else if(curr.getLeft()== prev){
				if(curr.getRight()!=null){
					s.push(curr.getRight());
				}
			}else{
				result.add(curr.getData());
				s.pop();
			}
			prev = curr;
			if(s.size() > maxDepth)
				maxDepth = s.size();
		}
		System.out.println("Max Depth --> "+maxDepth);
		return result;
	}
}
