package trees;

import java.util.ArrayList;
import java.util.Stack;

public class InorderBT {
	public void InorderRecursive(BinaryTreeNode root){
		if(root!=null){
			InorderRecursive(root.getLeft());
			System.out.print(root.getData()+"  ");
			InorderRecursive(root.getRight());
		}
	}
	public int sum = 0;
	public void InorderRecursive(BinaryTreeNode root, int isLeft){
		if(root!=null){
			if(isLeft == 1){
				sum = sum + root.getData();
			}
			InorderRecursive(root.getLeft(), 1);
			InorderRecursive(root.getRight(), 0);
		}
	}
	
	public ArrayList<Integer> InorderIterative(BinaryTreeNode root){
		if(root==null){
			return new ArrayList<Integer>();
		}
		ArrayList<Integer> result = new ArrayList<Integer>();
		Stack<BinaryTreeNode> s = new Stack<BinaryTreeNode>();
		BinaryTreeNode current = root;
		Boolean done = false;
		while(!done){
			if(current != null){
				s.push(current);
				current = current.getLeft();
			}
			else{
				if(s.isEmpty()){
					done = true;
				}
				else{
					current = s.pop();
					result.add(current.getData());
					current = current.getRight(); 
				}
			}
		}
		return result;
	}
}
