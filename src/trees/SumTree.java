package trees;

public class SumTree {
	public static void main(String[] args) {
		GenerateBT bt = new GenerateBT();
		PostOrderBT in = new PostOrderBT();
		in.PostOrderRecursive(bt.getRoot());
		System.out.println("");
		sumTree(bt.getRoot());
		in.PostOrderRecursive(bt.getRoot());
	}
	
	public static int sumTree(BinaryTreeNode root){
		if(root!=null){
			int left = sumTree(root.getLeft());
			int right = sumTree(root.getRight());
			int retRes = root.getData() + left + right;
			root.setData(left+right);
			return retRes;
		}
		return 0;
	}
}
