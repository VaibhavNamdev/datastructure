package trees;

public class Diameter {
	private int diameter(BinaryTreeNode root, Height height){
		Height lh = new Height();
		Height rh = new Height();
	
		if(root == null){
			height.h = 0;
			return 0;
		}
		
		lh.h++; 
		rh.h++;
		
		int diameterLeft = diameter(root.getLeft(), lh);
		int diameterRight = diameter(root.getRight(), rh);
		
		height.h = Math.max(lh.h, rh.h)+ 1;
		return Math.max(lh.h+rh.h+1, Math.max(diameterLeft, diameterRight));
		
	}
	private class Height{
		int h;
	}
	public int diameterOptimized(BinaryTreeNode root){
		Height h = new Height();
		int result = diameter(root, h);
		return result;
	}
	
	public int weidthBT(BinaryTreeNode root){
		int max = Integer.MIN_VALUE;
		int k = heightBT(root);
		for(int i=0;i<=k;i++){
			int temp = weidth(root, i);
			if(temp>max){
				max = temp;
			}
		}
		return max;
	}
	
	private int weidth(BinaryTreeNode root, int depth){
		if(root == null){
			return 0;
		}
		else{
			if(depth == 0){
				return 1;
			}
			return weidth(root.getLeft(), depth-1)+ weidth(root.getRight(), depth-1);
		}
	}
	
	private int heightBT(BinaryTreeNode root){
		if(root == null ){
			return 0;
		}
		return Math.max(heightBT(root.getLeft()), heightBT(root.getRight()))+1;
	}
	
	private int diameter = 0;
	public int diameterOfTree(BinaryTreeNode root){
		int left, right;
		if(root == null)
			return 0;
		left = diameterOfTree(root.getLeft());
		right = diameterOfTree(root.getRight());
		if(left+right>diameter){
			diameter = left + right;
		}
		return Math.max(left, right)+1;
	}
}

