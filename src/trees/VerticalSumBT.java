package trees;

import java.util.HashMap;
import java.util.TreeMap;

public class VerticalSumBT {
	
	private static HashMap<Integer, Integer> hash;
	private static TreeMap<Integer, Integer> tmap;

	public static void main(String[] args) {
		hash = new HashMap<Integer, Integer>();
		tmap = new TreeMap<Integer, Integer>();
		GenerateBT bt = new GenerateBT();
		vSum(hash, bt.getRoot(), 0);
		if(!tmap.containsKey(0)){
			tmap.put(0, bt.getRoot().getData());
		}
		topView(tmap, bt.getRoot(), 0);

		for(int h: hash.keySet()){
			System.out.println("Level "+h+" = "+hash.get(h));
		}
		System.out.println("  ");
		for(int t: tmap.keySet()){
			System.out.print(tmap.get(t)+" - ");
		}
	}
	
	public static void vSum(HashMap<Integer, Integer> hash, BinaryTreeNode root, int c){
		if(root.getLeft()!=null)
			vSum(hash, root.getLeft(), c-1);
		if(root.getRight()!=null)
			vSum(hash, root.getRight(), c+1);
		int data = 0;
		if(hash.containsKey(c))
			data = hash.get(c);
		hash.put(c, data+root.getData());
	}
	
	public static void topView(TreeMap<Integer, Integer> tmap, BinaryTreeNode root, int c){
		
		if(root.getLeft()!=null){
			topView(tmap, root.getLeft(), c-1);
			if(!tmap.containsKey(c))
				tmap.put(c, root.getData());
		}
		if(root.getRight()!=null){
			topView(tmap, root.getRight(), c+1);
			if(!tmap.containsKey(c))
				tmap.put(c, root.getData());
		}
	}
}
