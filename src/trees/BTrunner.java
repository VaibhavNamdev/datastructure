package trees;

import java.util.ArrayList;

import javax.crypto.spec.PSource;

public class BTrunner {

	public static void main(String[] args) {
		GenerateBT bt = new GenerateBT();
		PreorderBT preOrder = new PreorderBT();
		ArrayList<Integer> result = preOrder.PreorderIterative(bt.getRoot());
		Diameter d = new Diameter();
		System.out.println("Diameter -- >"+d.weidthBT(bt.getRoot()));
		InorderBT inOrder = new InorderBT();
		System.out.println(inOrder.sum);
		inOrder.InorderRecursive(bt.getRoot(), 0);
		System.out.println(inOrder.sum);
//		System.out.println("<-- Preorder -->");
//		for(int i : result){
//			System.out.print(i+"  ");
//		}
//		System.out.println("");
//		preOrder.PreorderRecursive(bt.getRoot());
//		
//		result.clear();
//		System.out.println("\n<-- Inorder -->");
//		InorderBT inOrder = new InorderBT();
//		result = inOrder.InorderIterative(bt.getRoot());
//		for(int i : result){
//			System.out.print(i+"  ");
//		}
//		System.out.println("");
//		inOrder.InorderRecursive(bt.getRoot());
//		
//		result.clear();
//		System.out.println("\n<-- PostOrder -->");
//		PostOrderBT postOrder = new PostOrderBT();
//		result = postOrder.PostOrderIterative(bt.getRoot());
//		for(int i : result){
//			System.out.print(i+"  ");
//		}
//		System.out.println("");
//		postOrder.PostOrderRecursive(bt.getRoot());
		
		result.clear();
//		System.out.println("\n<-- Level Order Traversal -->");
		LevelOrderBT levelOrder = new LevelOrderBT();
//		result = levelOrder.LevelOrderTraversal(bt.getRoot());
//		for(int i : result){
//			System.out.print(i+"  ");
//		}
//		System.out.println("");
//		System.out.println("Max Value by Recursion --> "+levelOrder.FindMaxRecursive(bt.getRoot()));
//		System.out.println("Search Result --> "+levelOrder.SearchBT(bt.getRoot(), 9));
//		System.out.println("Size of BT --> "+levelOrder.SizeofBTRecursive(bt.getRoot()));
//		System.out.println("Height/Depth of BT --> "+levelOrder.DepthofBTRecursive(bt.getRoot()));
//		System.out.println("Diameter of Tree --> "+levelOrder.diameterAlternate(bt.getRoot()));
//		
//		System.out.println("Optimized --> "+ new Diameter().diameterOptimized(bt.getRoot()));
//		System.out.println("Weidth of Tree --> "+new Diameter().weidthBT(bt.getRoot()));
		
//		System.out.println("PATHS of BT \n");
//		levelOrder.printTreePaths(bt.getRoot());
//		System.out.println("\n----------------------");
		
//		System.out.println("\nPath Sum is 18 --> "+levelOrder.hasPathSum(bt.getRoot(), 18));
//		System.out.println("Path Sum is 10 --> "+levelOrder.hasPathSum(bt.getRoot(), 10));
		System.out.println("Path Sum is 8 --> "+levelOrder.hasPathSum(bt.getRoot(), 8));
		
		
	}

}
