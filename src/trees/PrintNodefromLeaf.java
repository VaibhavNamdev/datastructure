package trees;

public class PrintNodefromLeaf {
	public static void main(String[] args) {
		GenerateBT bt = new GenerateBT();
		System.out.println(bt.getRoot().getData());
//		printNodefromLeaf(bt.getRoot(), 3);
		printNodefromRoot(bt.getRoot(), 3);
	}
	
	public static int printNodefromLeaf(BinaryTreeNode root, int k){
		if(root==null){
			return 0;
		}
		else{
			int left = printNodefromLeaf(root.getLeft(), k);
			int right = printNodefromLeaf(root.getRight(), k);
			if(left == k || right == k)
				System.out.print(root.getData()+"  ");
			return Math.max(left, right)+1;
		}
	}
	
	public static void printNodefromRoot(BinaryTreeNode root, int k){
		if(k==0 || root == null){
			if(root != null)
				System.out.print(root.getData()+"  ");
		}
		else{
			printNodefromRoot(root.getLeft(), k-1);
			printNodefromRoot(root.getRight(), k-1);
		}
	}
}
