import java.util.HashMap;
import java.util.Map;


public class TrialPay {
	public static void main(String[] args) {
		String a = "abc";
		String b = "cba";
		System.out.println(isPerm(a,b));
	}
	
	static boolean isPerm(String a, String b) {
		if(a.length() != b.length())
			return false;
		
		HashMap<Character, Integer> aMap = new HashMap<Character, Integer>();
		HashMap<Character, Integer> bMap = new HashMap<Character, Integer>();
		
		for(int i=0; i<a.length(); i++) {
			char aChar = a.charAt(i);
			char bChar = b.charAt(i);
			
			aMap.put(aChar, getVal(aMap, aChar) + 1);
			bMap.put(bChar, getVal(bMap, bChar) + 1);
		}
		
		for(int i=0; i<a.length(); i++) {
			char aChar = a.charAt(i);
			
			if(getVal(aMap, aChar) != getVal(bMap, aChar))
				return false;
		}
		
		return true;
	}
	
	static int getVal(Map<Character, Integer> m, char k) {
		if(m.get(k) == null)
			return 0;
		
		return m.get(k);
	}
}
