
public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		factorialIterative(3);
		System.out.println(factorialRecursive(5));
		
	}
	
	public static void factorialIterative(int number){
		long factorial = 1;;
		for(int i=number;i>0;i--)
			factorial = factorial*i;
		System.out.println("Factorial of "+number+" is = "+factorial);
	}
	
	public static int factorialRecursive(int number){
		if(number<2)
			return 1;
		return number * factorialRecursive(number-1);
	}
	
	

}
