
public class ReverseRecursive {
	private LLNode temp;
	public LLNode start;
	public LLNode RR(LLNode node){
		if(node.getNext()==null){
			start = node;
			return node;
		}
		temp = RR(node.getNext());
		node.setNext(null);
		temp.setNext(node);
		temp = node;
		return temp;
	}
	public LLNode RRR(LLNode node){
		RR(node);
		return start;
	}
}
