import java.util.ArrayList;
import java.util.Iterator;


public class LLLenght {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InsertDataLL idl = new InsertDataLL();
		LLNode head = idl.setDataLL();
		System.out.println("Lenght of LL --> " + listLength(head));
//		InsertNode in = new InsertNode();
//		head = in.InsertNode(head, 9, 12);
		ArrayList al = printList(head);
		System.out.println("Before reversing: "+ al.toString());
//		head = reverseLL(head);
//		al = printList(head);
//		System.out.println("After reversing: "+ al.toString());
//		System.out.println(findloop(head));
		ReverseRecursive rr = new ReverseRecursive();
		head = rr.RRR(head);
		al = printList(head);
		System.out.println(al.toString());
		
	}
	
	public static int listLength(LLNode head){
		LLNode ll = head;
		int length = 0;
		if(ll==null){
			return 0;
		}
		length ++;
		while(ll.getNext()!=null){
			ll = ll.getNext();
			length++;
		}
		return length;
	}
	
	public static ArrayList printList(LLNode head){
		ArrayList list = new ArrayList();
		while(head!=null){
			list.add(new Integer(head.getDataI()));
			head = head.getNext();
		}
		return list;
		
	}
	
	public static boolean findloop(LLNode head){
		LLNode slow = head;
		LLNode fast = head;
		while(fast!= null && fast.getNext() != null){
			fast = fast.getNext().getNext();
			slow = slow.getNext();
			if(fast == slow){
				return true;
			}
		}
		return false;
	}
	
	public static LLNode reverseLL(LLNode head){
		LLNode temp = null;
		LLNode prev = head;
		LLNode curr;
		while(prev!=null){
			curr = prev.getNext();
			prev.setNext(temp);
			temp = prev;
			prev = curr;
			
		}
		
		return temp;
	}

}
