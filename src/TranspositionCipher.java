import java.util.Arrays;
import java.util.Scanner;


public class TranspositionCipher {
	public static void main(String args[]) 
	{
	String key;
	String message;
	String encryptedMessage; 
	// Letters in the x-axis
	int x=0; 
	// Letters in the y-axis
	int y=0; 


	// Prompt the user
	System.out.print( "Type your Key : " );


	// Read a line of text from the user.

	Scanner scan = new Scanner(System.in);
	key = scan.nextLine();

	// Display the input back to the user.
	System.out.println( "Your Key is " + key );


	//Prompt the user
	System.out.print( "Type your Message : " );
	

	//Read a line of text from the user.
	message = scan.nextLine();

	//Display the input back to the user.
	System.out.println( "Your Message is " + message );

	
	int msgchar = message.length();
	int keycahr = key.length();
	char[] extra = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o'};
	int e = 0;
	if (!((msgchar % keycahr) == 0)){
	do{
	message = message + extra[e];
	e++;
	msgchar = message.length();
	}while(!((msgchar % keycahr) == 0));

	}


	encryptedMessage = "";

	// To set the temp as [x][y]
	char temp[][]=new char [key.length()][message.length()];
	char msg[] = message.toCharArray();
	// To populate the array
	x=0;
	y=0;
	// To convert the message into an array of char
	for (int i=0; i< msg.length;i++)
	{
	temp[x][y]=msg[i];
	if (x==(key.length()-1)) 
	{
	x=0;
	y=y+1;
	} // Close if 
	else 
	{
	x++;
	}
	} // Close for loop

	// To sort the key
	char t[]=new char [key.length()];
	t=key.toCharArray();
	Arrays.sort(t);

	for (int j=0;j<y;j++)
	{
	for (int i=0;i<key.length();i++)
	{
	System.out.print(temp[i][j]);
	}
	System.out.println();
	}

	System.out.println();

//	// To print out column by column (i.e. y)
	int[] position = {1,2,5,3,4};
	
	for (int i=0;i<key.length();i++){ 
	int priority = position[i]-1;
//	System.out.println(priority);
	for (int j=0;j<y;j++){ 
	System.out.println(priority+" "+j);
	encryptedMessage+=temp[priority][j]; 

	}
	System.out.println();
	}

	System.out.println(encryptedMessage);
	System.exit(0);
	}
}
