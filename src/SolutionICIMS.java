
public class SolutionICIMS {
	public static void main(String[] args) {
		int [] A = {4,3,2,1,5,7,6};
		System.out.println(solution(A));
	}
	
	public static int solution(int[] A){
		int length = A.length;		
		int total, sum = 0, result=0;
		for(int i=0; i<length; i++){
			sum = A[i] + sum;
			total = ((i+1)*(i+2))/2;
			System.out.println(total+" "+A[i]);
			if(total == sum)
				result++;
		}		
		return result;
	}
	
}
