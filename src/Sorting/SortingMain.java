package Sorting;

import java.util.Arrays;

public class SortingMain {
	public static void main(String[] args) {
		int [] A = {4,5,3,2,1};
		InsertionSort is = new InsertionSort();
		System.out.println(Arrays.toString(is.insertionSort(A)));
	}
}
