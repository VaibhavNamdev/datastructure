package Sorting;

public class InsertionSort {
	public int[] insertionSort(int A[]){
		int i, j, temp;
		for(i=2;i<A.length;i++){
			temp = A[i];
			j = i;
			while(j>=1){
				if(A[j-1] > temp){
					A[j] = A[j-1];
					j--;
				}else
					break;
			}
			A[j] = temp;
		}
		return A;
	}
}
