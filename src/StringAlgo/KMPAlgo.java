package StringAlgo;

public class KMPAlgo {
	public static void main(String[] args) {
		String text = "bacbabababacaca";
		String pattern = "ababaca";
		char [] T = text.toCharArray();
		char [] P = pattern.toCharArray();
		int n = T.length;
		int m = P.length;
		System.out.println(KMPStringSearch(T, n, P, m));
	}
	
	private static int[] createPattern(char[] P, int m){
		int [] pattern = new int[m];
		int i = 1, j=0;
		pattern[j] = 0; 
		while(i < m){
			if(P[i]==P[j]){
				pattern[i] = j+1;
				i++;
				j++;
			}
			else if(j>0){
				j = pattern[j-1];
			}
			else{
				pattern[i] = 0;
				i++;
			}
		}
		
		return pattern;
	}
	
	public static int KMPStringSearch(char[] T, int n, char [] P, int m){
		int [] key = createPattern(P, m);
		int i=0, j=0;
		while(i<n){
			if(T[i]==P[j]){
				if(j == (m-1) )
					return i-j;
				else{
					i++;
					j++;
				}
			}
			else if(j>0)
				j = key[j-1];
			else
				i++;
		}
		return -1;
	}
}
