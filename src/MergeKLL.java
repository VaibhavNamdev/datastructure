import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;


public class MergeKLL {
	public static void main(String[] args) {
		InsertDataLL idl = new InsertDataLL();InsertDataLL id2 = new InsertDataLL();InsertDataLL id3 = new InsertDataLL();InsertDataLL id4 = new InsertDataLL();
		LLNode head1 = idl.setDataLL();LLNode head2 = id2.setDataLL();LLNode head3 = id3.setDataLL();	LLNode head4 = id4.setDataLL();

		ArrayList<LLNode> list = new ArrayList<LLNode>();
		list.add(head1);list.add(head2);list.add(head3);list.add(head4);
		
		LLNode result = mergeKList(list);
//		LLLenght ll = new LLLenght();
//		System.out.println("After Merging: "+ ll.printList(result).toString());
	}
	
	public static LLNode mergeKList(ArrayList<LLNode> list){
		LLNode head = null, curr = null;
		if(list.isEmpty())
			return null;
		PriorityQueue<LLNode> Pq = new PriorityQueue<LLNode>(new Comparator<LLNode>() {
			public int compare(LLNode o1, LLNode o2){
				return o1.getDataI() - o2.getDataI();
			}
		});
		for(LLNode node:list){
			if(node!=null)
				Pq.add(node);
		}
		while(!Pq.isEmpty()){
			if(head==null){
				System.out.print(Pq.peek().getDataI()+"->");
				head = Pq.poll();
				curr = head;
			}else{
				System.out.print(Pq.peek().getDataI()+"->");
				curr.setNext(Pq.poll());
				curr = curr.getNext();
			}
			if(curr.getNext()!=null){
				Pq.add(curr.getNext());
			}
		}
		System.out.println("null");
		return head;
	}
}
