package Array;

import java.util.Arrays;

public class RotateMatrix {
	public static void main(String[] args) {
	    int mat[][] = { {1,  2,  3,  4},
	            {5,  6,  7,  8},
	            {9,  10, 11, 12},
	            {13, 14, 15, 16}  };
	    
	    mat = rotateMatrix(mat);
	    for(int[] i : mat){
	    	System.out.println(Arrays.toString(i));
	    }
	}
	
	public static int[][] rotateMatrix(int [][] mat){
		int row = 0, col = 0;
		int prev, curr;
		int m = mat.length;
		int n = mat[0].length;
		System.out.println("Rows => "+m+" Coloumns => "+n);
		while(row < m && col < n){
			if(row+1 == m || col+1 == n)
				break;
			
			prev = mat[row+1][col];
			
			for (int i = col; i < n; i++) {
				curr = mat[row][i];
				mat[row][i] = prev;
				prev = curr;
			}
			row++;
			
			for(int i = row; i< m; i++){
				curr = mat[i][n-1];
				mat[i][n-1] = prev;
				prev = curr;
			}
			n--;
			
			if(row < m){
				for(int i = n-1; i >= col; i--){
					curr = mat[m-1][i];
					mat[m-1][i] = prev;
					prev = curr;
				}
			}
			m--;
			
			if(col < n){
				for(int i = m-1; i >= row; i--){
					curr = mat[i][col];
					mat[i][col] = prev;
					prev = curr;
				}
			}
			col++;
		}
		
		
		return mat;
	}
}
