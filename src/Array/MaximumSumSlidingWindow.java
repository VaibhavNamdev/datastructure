package Array;

import java.util.*;
public class MaximumSumSlidingWindow {
	public static void main(String[] args) {
		int[] A = {1,3,-1,-3,5,3,6,7};
		int window = 3;
		int [] B = new int[A.length-window+1];
		B = maxSumSliding(A, window, B);
		System.out.println(Arrays.toString(B));
	}
	
	public static int[] maxSumSliding(int[] A, int w, int [] B){
		Deque<Integer> queue = new ArrayDeque<Integer>(A.length);
		for(int i=0;i<w;i++){
			while(!queue.isEmpty() && A[i]>=queue.getLast()){
				queue.pollLast();
			}
			queue.addLast(i);
			System.out.println(queue.toString());
		}
		for(int i=w;i<A.length;i++){
			B[i-w] = A[queue.getFirst()];
			while(!queue.isEmpty() && A[i]>=queue.getLast()){
				queue.pollLast();
			}
//			System.out.println("1st-->"+queue.toString());
			while(!queue.isEmpty() && queue.getFirst()<=i-w){
				queue.pollFirst();
			}
//			System.out.println("2nd-->"+queue.toString());
			queue.addLast(i);
			System.out.println(queue.toString());
		}
		B[A.length-w]=A[queue.peekFirst()];
		return B;
	}
}
