package Array;

import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;

public class TicketSelling {
	public static void main(String[] args) {
		int [] window = { 5, 1, 7, 10, 11, 9 };
		int noOfTickets = 5;
		System.out.println("Max Profit => "+maxProfit(window, noOfTickets));
		
	}
	
	public static int maxProfit(int [] window, int n){
		int maxProfit = 0;
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>(new Comparator<Integer>() {
			public int compare(Integer o1, Integer o2){
				return ((o2-o1));
			}
		});
		for(int i: window)
			pq.offer(i);
		
		while(n > 0){
			int ticket = pq.poll();
			System.out.println(ticket);
			maxProfit += ticket;
			if((ticket-1)!=0){
				pq.offer(ticket-1);
			}
			n--;
		}
		
		return maxProfit;
	}
}
