package Array;

import java.util.Arrays;

public class SwapElementinArray {
	public static void main(String[] args) {
		int A[] = {1,2,3,4,5,6};
		int i=0, j=0;
		A = swap(A, i,j);
		System.out.println(Arrays.toString(A));
		A = reverseArray(A);
		System.out.println(Arrays.toString(A));
	}
	
	public static int[] swap(int [] A, int i, int j){
		if(i<A.length && j<A.length && j!=i){
			A[i] = A[i]^A[j];
			A[j] = A[i]^A[j];
			A[i] = A[i]^A[j];
		}
		return A;
	}
	
	public static int[] reverseArray(int[] A){
		int i=0, j=(A.length-1);
		while(i<j){
			A[i] = A[i]^A[j];
			A[j] = A[i]^A[j];
			A[i] = A[i]^A[j];
			j--;
			i++;
		}
		return A;
	}
}
