package Array;

import java.util.Arrays;

public class RotateArray {
	public static void main(String[] args) {
		int [] A ={1,2,3,4,5,6,7,8,9,10,11,12,13,14};
		int d = 6;
		A = (rotateArrayJuggling(A, A.length, d));
		System.out.println(Arrays.toString(A));
	}
	
	public int[] rotateArray(int [] A, int d, int r){
		int n = A.length;
		for(int i=0;i<d;i++){
			if(r==0)
				A = leftRotateByOne(A, n);
			else
				A = rightRotateByOne(A, n);
		}
		return A;
	}
	
	private static int[] leftRotateByOne(int []A, int n){
		int i, temp = A[0];
		for(i=0; i<(n-1);i++)
			A[i] = A[i+1];
		A[i]=temp;
		return A;
	}
	
	private static int[] rightRotateByOne(int []A, int n){
		int i, temp=A[n-1];
		for(i=n-2; i>=0; i--)
			A[i+1] = A[i];
		A[i+1] = temp;
		return A;
	}
	
	public static int[] rotateArrayJuggling(int[] arr, int n, int d){
		int i, j , k, temp;
		System.out.println(gcd(d,n));
		 for (i = 0; i < gcd(d, n); i++)
		  {
		    /* move i-th values of blocks */
		    temp = arr[i];
		    j = i;
		    while(true)
		    {
		      k = j + d;
		      if (k >= n)
		        k = k - n;
		      if (k == i)
		        break;
		      arr[j] = arr[k];
		      j = k;
		    }
		    arr[j] = temp;
		  }
		 return arr;
	}
	
	public static int gcd(int a,int b)
	{
	   if(b==0)
	     return a;
	   else
	     return gcd(b, a%b);
	}
	
	public int gcd2(int a, int b){
		while(a!=b){
			if(a > b)
				a = a-b;
			else 
				b = b-a;
		}
		return a;
	}
	
	public int gcd3(int a, int b){
		int t;
		while (b != 0){
			t = b; 
			b = a % b; 
			a = t; 
		}
    	return a;
	}
}
