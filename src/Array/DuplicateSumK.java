package Array;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class DuplicateSumK {
	public static void main(String[] args) {
		int [] A = {7,7,7,2,3,1,2,1,1,0,2,3,4,6,6,6,6,7,8,2,3};
		int k = 3;
		System.out.println(duplicateK(A, k));
		Arrays.sort(A);
		System.out.println(removeDuplicateFromSorted(A, 2).toString());
	}
	
	public static boolean duplicateK(int [] A, int k){
		HashMap<Integer, Integer> hash = new HashMap<Integer, Integer>();
		boolean bool = false;
		for(int i=0; i< A.length; i++){
			if(hash.containsKey(A[i])){
				int j = hash.get(A[i]);
				if(i-j > k)
					hash.put(A[i], i);
				else if(i-j<k)
					continue;
				else{
					System.out.println(i+1 + " and " + (j+1));
					bool = true;
				}
			}
			else
				hash.put(A[i], i);
		}
		return bool;
	}
	
	public static ArrayList<Integer> removeDuplicateFromSorted(int [] A, int k){
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(A[0]);
		boolean bool = true;
		int counter = 1;
		for(int i=1; i<A.length; i++){
			if(A[i-1] != A[i]){
				counter = 1;
				list.add(A[i]);
			} else if( counter < k ){
				counter++;
				list.add(A[i]);
			}
			
		}
		
		return list;
	}
}
