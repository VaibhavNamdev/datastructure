package Array;

public class ConsecutiveIntegers {
	public static void main(String[] args) {
		int [] A = {1,3,4,5};
		System.out.println(checkConsecutive(A));	
		int [] arrA = {21,24,22,26,23,25};
		System.out.println(checkConsecutive(arrA));
		int [] arrB = {11,10,12,14,13};
		System.out.println(checkConsecutive(arrB));
		int [] arrC = {11,10,14,13};
		System.out.println(checkConsecutive(arrC));
	}
	
	public static boolean checkConsecutive(int []A){
		//find min and max
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int total = 0;
		for(int i=0; i<A.length; i++){
			if(A[i]>max)
				max = A[i];
			if(A[i]<min)
				min = A[i];
			total += A[i];
		}
		long sum1 = (max*(max+1))/2;
		long sum2 = ((min-1)*(min))/2;
		sum1 = sum1-sum2;
		if(total == sum1)
			return true;
		return false;
		
	}
}
