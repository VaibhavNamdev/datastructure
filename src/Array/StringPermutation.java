package Array;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StringPermutation {
	public static void main(String[] args) {
		String s = "AABC";
		char [] input = s.toCharArray();
		permute(input);
	}
	
	public static void permute(char [] text){
		Map<Character,Integer> map = new HashMap<Character, Integer>();
		for(char c : text){
			if(map.containsKey(c)){
				int count = map.get(c);
				map.put(c, count+1);
			}
			else{
				map.put(c, 1);
			}
		}
		
		char [] str = new char[map.size()];
		int [] count = new int[map.size()];
		int index = 0;
		for(Map.Entry<Character, Integer> entry : map.entrySet()){
			str[index] = entry.getKey();
			count[index] = entry.getValue();
			index++;
		}
		
		char result[] = new char[text.length];
		System.out.println(Arrays.toString(str));
		System.out.println(Arrays.toString(count));
		permuteUtil(str, count, result, 0);
		
	}
	
	public static int counter=0;
	private static void permuteUtil(char[] str, int[] count, char[] result, int level){
		if(level == result.length){
			System.out.println((++counter)+" : "+Arrays.toString(result));
			return;
		}
		
		for(int i=0;i<str.length;i++){
			if(count[i] == 0)
				continue;
			result[level] = str[i];
			count[i]--;
			permuteUtil(str, count, result, level+1);
			count[i]++;
		}
	}
}
