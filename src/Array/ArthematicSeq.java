package Array;

public class ArthematicSeq {
	public static void main(String[] args) {
		int A[] = {2,5,8,11};
		System.out.println(isSequence(A));
	}
	
	public static boolean isSequence(int [] A){
		int n = A.length, arraySum=0;
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		
		for(int i=0; i<n ; i++){
			arraySum += A[i];
			if(A[i] > max)
				max = A[i];
			if(A[i] < min)
				min = A[i];
		}
		
		int sum = (n*(min+max))/2;
		if(arraySum == sum)
			return true;
		return false;
	}
}
