package Array;

public class MaximumProfit {
	public static void main(String[] args) {
		int [] A = {2,5,3,2,1,2,3};
		System.out.println("Maximum Profit from one buy and sell ==> "+maxProfitOneBuySell(A));
		System.out.println("Maximum Profit from buy and sell multiple ==> "+maxProfitMultipleBS(A));
	}
	
	public static int maxProfitOneBuySell(int [] A){
		int minElement = Integer.MAX_VALUE;
		int profit = 0;
		for (int i = 0; i < A.length; i++) {
			profit = Math.max(profit, A[i] - minElement);
			minElement = Math.min(minElement, A[i]);
		}
		return profit;
	}
	
	public static int maxProfitMultipleBS(int [] A){
		int profit = 0;
		for (int i = 1; i < A.length; i++) {
			int diff = A[i]-A[i-1];
			if(diff>0)
				profit += diff;
		}
		return profit;
	}

}
