package Array;
//Maximum sum contiguous subarray
public class KadanesAlgo {
	public static void main(String[] args) {
		int [] A = {0,-1,2,-3,5,9,-5,10};
		System.out.println("Maximum contiguous sum in subarray ==> "+kadane(A));
	}
	
	public static int kadane(int [] A){
		int max_so_far = 0;
		int max_ending_here = 0;
		int indexStart = 0, indexEnd = 0;
		for(int i=0;i<A.length;i++){
			max_ending_here = max_ending_here + A[i];
			if(max_ending_here<0){
				max_ending_here=0;
				indexStart = i+1;
			}
			if(max_ending_here>max_so_far){
				indexEnd = i;
				max_so_far = max_ending_here;
			}
		}
		if(indexStart == A.length)
			indexStart = 0;
		System.out.println(indexStart+" , "+indexEnd);
		return max_so_far;
	}
}
