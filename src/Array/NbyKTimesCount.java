package Array;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class NbyKTimesCount {
	public static void main(String[] args) {
		int [] arr = {2, 2,  4, 4, 3, 5, 3, 4, 4, 6, 4, 3, 3, 8, 8, 8};
		ArrayList<Integer> result = nbyKtimes(arr);
//		Collections.reverse(result);
		if(!result.isEmpty())
			System.out.println(result.toString());
		else
			System.out.println("No result found");
	}
	
	public static ArrayList<Integer> nbyKtimes(int [] arr){
		ArrayList<Integer> result = new ArrayList<Integer>();
		HashMap<Integer, Integer> hash = new HashMap<Integer, Integer>();
		int N = arr.length;
		for(int i=0; i<N; i++){
			if(hash.containsKey(arr[i]))
				hash.put(arr[i], hash.get(arr[i]) + 1);
			else
				hash.put(arr[i], 1);
		}
		
		for(Map.Entry<Integer, Integer> map: hash.entrySet()){
			int calc = N/map.getKey();
			int value = map.getValue();
			System.out.println(map.getKey()+ " " +calc+ "   " + value );
			if(calc<value)
				result.add(map.getKey());
		}
		hash.clear();
		return result;
	}
}
