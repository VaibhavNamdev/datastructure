package Array;

import java.util.Stack;

public class MaximumAreaInHistogram {
	public static void main(String[] args) {
		int [] A = {2,1,5,6,2,3};
		System.out.println(maxSumInHistogram(A));
	}
	
	public static int maxSumInHistogram(int [] A){
		Stack<Integer> s = new Stack<Integer>();
		int i = 0;
		int curr_area = 0;
		int max_area = 0;
		int top = 0;
	
		while(i < A.length){
			if(s.isEmpty() || A[s.peek()]<=A[i]){
				s.push(i);
				i++;
			}
			else{
				top = s.pop();
				curr_area = A[top] * (s.isEmpty()?i:(i-s.peek()-1));
				max_area = Math.max(curr_area, max_area);
			}
		}
		
		while(!s.isEmpty()){
			top = s.pop();
			curr_area = A[top] * (s.isEmpty()?i:(i-s.peek()-1));
			max_area = Math.max(curr_area, max_area);
		}
		
		return max_area;
	}
}
