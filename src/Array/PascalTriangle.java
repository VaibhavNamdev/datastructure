package Array;

import java.util.ArrayList;

public class PascalTriangle {
	public static void main(String[] args) {
		int num = 5;
		ArrayList<ArrayList<Integer>> result = getPascalTriangle(num);
		for (ArrayList<Integer> i : result) {
			// Uncomment for pascals particular row
//			if(i.size() == num)
				System.out.println(i.toString());
		}
	}
	
	public static ArrayList<ArrayList<Integer>> getPascalTriangle(int num){
		ArrayList<Integer> pre = new ArrayList<Integer>();
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if(num <=0)
			return result;
		pre.add(1);
		result.add(pre);
		
		for (int i = 2; i <= num; i++) {
			ArrayList<Integer> curr  = new ArrayList<Integer>();
			curr.add(1);
			for (int j = 0; j < pre.size()-1; j++) {
				int data = pre.get(j) + pre.get(j+1);
				curr.add(data);
			}
			curr.add(1);
			result.add(curr);
			pre = curr;
		}
		
		return result;
	}
}
