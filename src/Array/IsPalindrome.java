package Array;

import dynamicProblems.LongestComSub;

public class IsPalindrome {
	public static void main(String[] args) {
		String s = "geeksskeeg";
		System.out.println("Longest Palindrome ==> "+longestPalindromeSubString(s));
		System.out.println(isPalindrome1(s)+"   "+isPalindrome2(s));
		s = "BOB";
		System.out.println(isPalindrome1(s)+"   "+isPalindrome2(s));
		s = "B";
		System.out.println(isPalindrome1(s)+"   "+isPalindrome2(s));
		
		
	
		
	}
	
	public static boolean isPalindrome1(String s){
		int len = s.length()-1;
		for(int i=0 ; i<=len; i++){
			if(s.charAt(i) != s.charAt(len-i))
				return false;
		}
		
		return true;
	}
	
	public static boolean isPalindrome2(String s){
		char [] a = s.toCharArray();
		int len = a.length-1;
		for (int i = 0; i < a.length; i++) {
			if(a[i] != a[len-i])
				return false;
		}
		
		return true;
	}
	
	public static String longestPalindromeSubString(String s){
		int len = s.length();
		String longest = null;
		int maxLength = 0;
		for (int i = 0; i < len; i++) {
			for (int j = i; j < len; j++) {
				String curr = s.substring(i, j+1);
				if(isPalindrome1(curr) && maxLength <= j-i){
						maxLength = j-i;
						longest = curr;
				}
			}
		}
		
		return longest;
	}
	
}
