package Array;

import java.util.Arrays;

public class NLengthStrings {
	public static int count = 0;
	public static void main(String[] args) {
		String input = "ALGO";
//		printNLengthFromK(2, input.toCharArray(), new char[2]);
		System.out.println("\n"+count);
//		printNLengthInt(2, 3, new int[2]);
		printNString2(2, new char[2], input.toCharArray());
	}
	
	public static void printNLengthFromK(int n, char [] k, char [] A){
		if(n<=0){
			count++;
			System.out.print(String.valueOf(A)+" ");
		}
		else{
			for(int i=0; i<k.length; i++){
				A[n-1] = k[i];
				printNLengthFromK(n-1, k, A);
			}
		}
	}
	
	public static void printNLengthInt(int n, int k, int [] A){
		if(n<=0)
			System.out.print(Arrays.toString(A)+" ");
		else{
			for(int i=0; i<k; i++){
				A[n-1] = i+1;
				printNLengthInt(n-1, k, A);
			}
		}
	}
	
	public static void printNString2(int n, char [] A, char [] K){
		if(n<=0)
			System.out.print(String.valueOf(A)+" ");
		
		else{
			for(int i=0; i<K.length;i++){
				A[n-1] = K[i];
				printNString2(n-1, A, K);
			}
		}
	}
	
	public static void printNInt(int n, int k, int[] A){
		if(n<=0)
			System.out.print(Arrays.toString(A));
		else{
			for(int i=1; i<=k; i++){
				A[n-1] = i;
				printNInt(n-1, k, A);
			}
		}
	}
}
