package Array;

public class FindMinInRotated {
	public static void main(String[] args) {
		int [] A = {4,5,5,6,6,7,1};
		int left = 0;
		int right = A.length-1;
		System.out.println("Minimum element in Rotated Array : ==> "+findMinInRotatedArray(A, left, right));
	}
	
	public static int findMinInRotatedArray(int [] A, int left, int right){
		if(left == right)
			return A[left];
		if(right-left == 1)
			return Math.min(A[left], A[right]);
		
		int mid = left + (right-left)/2;
		
		if(A[left] < A[right])
			return A[left];
		if(A[right] == A[left])
			return findMinInRotatedArray(A, left+1, right);
		else if(A[mid] >= A[left])
			return findMinInRotatedArray(A, mid, right);
		else
			return findMinInRotatedArray(A, left, mid);
	}
}
