import java.util.ArrayList;


public class MoveAlternateNode {
	public static void main(String[] args) {
		InsertDataLL idl = new InsertDataLL();
		LLNode head = idl.setDataLL();
		LLLenght ll = new LLLenght();
		ArrayList al = ll.printList(head);
		System.out.println("Before Shifting: "+ al.toString());
		head = moveAlternateNode(head);
		al = ll.printList(head);
		System.out.println("After Shifting: "+ al.toString());
		
	}
	
	public static LLNode moveAlternateNode(LLNode head){
		LLNode hEven = head;
		LLNode hOdd = null, temp = null;
		LLNode joinPoint = null;
		if(head.getNext()!=null){
			hOdd = head.getNext();
			joinPoint = hOdd;
			temp = hOdd.getNext();
		}
		
		while(temp!=null){
			hEven.setNext(temp);
			hOdd.setNext(temp.getNext());
			temp = temp.getNext();
			if(temp != null)
				temp = temp.getNext();
			hEven = hEven.getNext();
			hOdd = hOdd.getNext();
		}
//		System.out.println(joinPoint.);
//		System.out.println(joinPoint.getDataI());
		hEven.setNext(joinPoint);
		return head;
	}
}
