
public class InsertNode {
	private LLNode tmp;
	public LLNode InsertNode(final LLNode head, final int data, final int pos){
		this.tmp = head;
		int position = 1;
		if(pos == 1){
			LLNode newnode = new LLNode();
			newnode.setData(data);
			newnode.setNext(head);
			return newnode;
		}
		while(position!=(pos-1) && pos > 0 && tmp != null){
			tmp = tmp.getNext();
			position++;
		}
		if(tmp==null || pos < 1){
			System.out.println("Cant add!");
		}
		else{
			LLNode newnode = new LLNode();
			newnode.setData(data);
			newnode.setNext(tmp.getNext());
			tmp.setNext(newnode);
			return head;
		}
		return head;
		
	}
	
}
