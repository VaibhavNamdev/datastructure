public class InsertDataLL {
	
	LLNode ll = new LLNode();
	
	public LLNode setDataLL() {
		// TODO Auto-generated method stub
		ll.setData(1);
		LLNode ll1 = new LLNode();
		ll1.setData(2);
		LLNode ll2 = new LLNode();
		ll2.setData(3);
		LLNode ll3 = new LLNode();
		ll3.setData(4);
		LLNode ll4 = new LLNode();
		ll4.setData(5);
		LLNode ll5 = new LLNode();
		ll5.setData(6);
		LLNode ll6 = new LLNode();
		ll6.setData(7);
		
		ll.setNext(ll1);
		ll1.setNext(ll2);
		ll2.setNext(ll3);
		ll3.setNext(ll4);
		ll4.setNext(ll5);
		ll5.setNext(ll6);
		ll6.setNext(null);
		
		return ll;
		
	}
	
}
