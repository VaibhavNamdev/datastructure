package Fibonacci;

public class Fibonacci {
	
	public int[] fib;
	public void setfibArray(int n){
		this.fib = new int[n+1];
	}
	public int fibonacciRecursive(int n){
		if(n==0)
			return 0;
		if(n==1)
			return 1;
		return fibonacciRecursive(n-1)+fibonacciRecursive(n-2);
	}
	
	public int fibonacciOptimized(int n){
		int first = 0, second = 1, sum=0;;
		for(int i=2;i<=n;i++){
			sum = second + first;
			first = second;
			second = sum;
		}
		return sum;
	}
	
	public int fibonacciDynamicBU(int n){
		fib[0] = 0;
		fib[1] = 1;
		for(int i=2; i<=n ; i++)
			fib[i] = fib[i-1] + fib[i-2];
		return fib[n];
	}
	
	public int fibonacciDynamicTD(int n){
		if(n==1)
			return 1;
		if(n==2)
			return 1;
		if(fib[n]!=0)
			return fib[n];
		
		return fib[n]= fibonacciDynamicTD(n-1) + fibonacciDynamicTD(n-2);
	}
	
	public static void main(String[] args) {
		Fibonacci fib = new Fibonacci();
		System.out.println(fib.fibonacciOptimized(6));
		System.out.println(fib.fibonacciRecursive(6));
		fib.setfibArray(6);
		System.out.println(fib.fibonacciDynamicBU(6));
		System.out.println(fib.fibonacciDynamicTD(6));
	}

}
